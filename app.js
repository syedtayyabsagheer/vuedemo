const app = Vue.createApp({
   data() {
       return{
           width: 0,
           todoList : [
               {
                   title: "Hi TAYYAB READY TO REVISE LAST DAY CONCEPT",
                    author: 'YouTube',
                    status: false,
                    progress: 99,
                    showprogress: false
                },
                {
                    title: "Then i will do Practice the concept",
                     author: 'YouTube',
                     status: true,
                     progress: 0,
                     showprogress: false
                 },
                 {
                    title: "In oct i am going to marriage InshaALLAH",
                     author: 'YouTube',
                     status: true,
                     progress: 0,
                     showprogress: false
                 },

           ]
       }
      
   },
   methods:{
       toggleStatus(task){
           task.status = !task.status
       },
       toggleProgressStatus(task){
           task.showprogress =  !task.showprogress
       },
       updateProgress(task)
       {
           if(task.progress < 100)
           {
            task.progress++
            this.width = task.progress
           }      
       },
       minusProgress(task)
       {
           if(task.progress > 0)
           {
            task.progress--
            this.width = task.progress
           }
       },
       filterResult(){
        this.todoList = this.todoList.filter((task) => task.status)
       },
       notfilterResult()
       {
        this.todoList = [
            {
                title: "Hi TAYYAB READY TO REVISE LAST DAY CONCEPT",
                 author: 'YouTube',
                 status: false,
                 progress: 99,
                 showprogress: false
             },
             {
                 title: "Then i will do Practice the concept",
                  author: 'YouTube',
                  status: true,
                  progress: 0,
                  showprogress: false
              },
              {
                 title: "In oct i am going to marriage InshaALLAH",
                  author: 'YouTube',
                  status: true,
                  progress: 0,
                  showprogress: false
              },

        ]
       }
   }
})
app.mount("#app")
